@Library('pipeline-library') _

pipeline {
    agent any
    environment {
        APT_LOCK    = "apt-lock-${env.NODE_NAME}"
        IVERILOG_DIR    = 'iverilog'
    }
    stages {
        stage('Setup') {
            steps {
                echo 'Setting up ...'
                timeout(time: 10, unit: 'MINUTES') {
                    retry(5) {
                        lock("${env.APT_LOCK}") {
                            sh 'sudo ./setup.sh'
                        }
                    }
                }
            }
        }
        stage('Clean') {
            steps {
                echo 'Cleaning ...'
                sh "rm -rf ${env.IVERILOG_DIR}"
            }
        }
        stage('Fetch') {
            steps {
                echo 'Fetching ...'
                sh './fetch.sh'
            }
        }
        stage('Patch') {
            steps {
                echo 'Patching ...'
                sh "./patch.sh ${env.IVERILOG_DIR}"
            }
        }
        stage('Build') {
            environment {
                // CC and CXX are picked up by ./configure
                CC      = '/usr/lib/ccache/gcc'
                CXX     = '/usr/lib/ccache/g++'
            }
            steps {
                dir("${env.IVERILOG_DIR}") {
                    echo 'Building ...'
                    sh 'bash ./autoconf.sh'
                    sh './configure'
                    sh 'make -j$(nproc)'
                }
            }
        }
        stage('Test') {
            steps {
                dir("${env.IVERILOG_DIR}") {
                    echo 'Testing ...'
                    sh 'make -j$(nproc) cppcheck'
                }
            }
        }
    }
    post {
        always {
            commonStepNotification()
        }
    }
}
