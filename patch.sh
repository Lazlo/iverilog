#!/bin/sh

sed -i -E "s/(cppcheck) (--)/\1 -j$(nproc) \2/" $1/Makefile.in
