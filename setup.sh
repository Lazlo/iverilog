#!/bin/sh

pkgs=""
pkgs="$pkgs autoconf autoconf-archive"
pkgs="$pkgs make"
pkgs="$pkgs gcc"
pkgs="$pkgs g++"
pkgs="$pkgs cppcheck"
pkgs="$pkgs gperf"
pkgs="$pkgs flex"

apt-get -q install -y $pkgs
